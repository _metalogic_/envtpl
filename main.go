package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"

	"bitbucket.org/_metalogic_/log"
)

var (
	inFlg string
	text  string
	templ string
)

func init() {
	flag.Usage = func() {
		fmt.Printf("usage: envtpl -in - | -in filename\n---\n")
	}
	flag.StringVar(&inFlg, "in", "", "the template to render")
	log.SetFlags(0)
}

func main() {
	flag.Parse()

	if inFlg == "" {
		flag.Usage()
		log.Fatal("a template must be provided")

	}

	if inFlg == "-" { // read template from standard input
		templ = "stdin"
		if b, err := io.ReadAll(os.Stdin); err == nil {
			text = string(b)
		}
	} else {
		templ = filepath.Base(inFlg)
		b, err := ioutil.ReadFile(inFlg)
		if err != nil {
			log.Fatalf("failed reading file %s (%s)\n", inFlg, err)
		}
		text = string(b)
	}

	tpl, err := createTpl(templ, text)
	if err != nil {
		log.Fatalf("failed parsing template %s (%s)\n", inFlg, err)
	}

	envMap := collectEnv()

	if err := renderTpl(tpl, os.Stdout, envMap); err != nil {
		log.Fatalf("failed rendering template (%s)", err)
	}
}
