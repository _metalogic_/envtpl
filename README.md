# envtpl

A clone and refresh of https://github.com/arschles/envtpl

Added:

* support for go mod
* reading template from STDIN

---

Render Go templates from Environment Variables. Let's say you have the following template file called `pwd.tpl`:

```
The current working directory is {{.PWD}}
```

Run `envtpl pwd.tpl`, and you'll see the following printed to STDOUT:

```
The current working directory is /path/you/executed/from
```
