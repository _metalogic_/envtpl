module bitbucket.org/_metalogic_/envtpl

go 1.17

require (
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/Masterminds/sprig/v3 v3.2.2
	github.com/stretchr/testify v1.7.0
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver/v3 v3.1.1 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.2.0 // indirect
	github.com/huandu/xstrings v1.3.2 // indirect
	github.com/imdario/mergo v0.3.12 // indirect
	github.com/mitchellh/copystructure v1.1.1 // indirect
	github.com/mitchellh/reflectwalk v1.0.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/spf13/cast v1.3.1 // indirect
	golang.org/x/crypto v0.0.0-20210317152858-513c2a44f670 // indirect
	golang.org/x/sys v0.0.0-20201119102817-f84b799fce68 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
