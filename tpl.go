package main

import (
	"io"
	"text/template"

	"github.com/Masterminds/sprig/v3"
)

func createTpl(tplName, text string) (*template.Template, error) {
	return template.New(tplName).Funcs(sprig.TxtFuncMap()).Parse(text)
}

func renderTpl(tpl *template.Template, w io.Writer, data interface{}) error {
	return tpl.Execute(w, data)
}
